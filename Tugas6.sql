SELECT MONTHNAME(`Date Received`) AS Month , COUNT(id) AS JumlahComplaint
FROM consumercomplaints GROUP BY MONTH(`Date Received`);

SELECT * FROM consumercomplaints WHERE Tags LIKE "%Older American%";

CREATE VIEW Closed AS
SELECT Company,COUNT(`Company Response to Consumer`) AS Closed
FROM consumercomplaints
WHERE `Company Response to Consumer`='Closed' GROUP BY Company;

CREATE VIEW ClosedwithExplanation AS
SELECT Company, COUNT(`Company Response to Consumer`) AS `Closed with Explanation`
FROM consumercomplaints
WHERE `Company Response to Consumer`='Closed with explanation' GROUP BY Company;

CREATE VIEW ClosedwithNonMonetary AS
SELECT Company,COUNT(`Company Response to Consumer`) AS `Closed with non-Monetary Relief`
FROM consumercomplaints
WHERE `Company Response to Consumer`='Closed with non-monetary relief' GROUP BY Company;

SELECT c.Company, c.Closed, ce.`Closed with Explanation`, cm.`Closed with non-Monetary Relief` FROM closed c
INNER JOIN closedwithexplanation ce
ON c.Company=ce.Company
INNER JOIN closedwithnonmonetary cm
ON c.Company=cm.Company